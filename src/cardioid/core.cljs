(ns ^:figwheel-hooks cardioid.core
  (:refer-clojure :exclude [ + - * /])
  (:require
   [clojure.pprint :refer [cl-format]]
   [clojure.core.async :as async :refer [chan go go-loop <! >! put! alt!]]

   [webjunk.bulma :as bulma]
   [webjunk.generic-math :refer [+ - * / abs sqrt pow exp log conjugate sgn sin cos]]
   [webjunk.complex :as complex :refer [i -i cis complex ensure-complex real imag]]
   [webjunk.dual :as dual :refer [dual tangent]]
   [webjunk.svg :as svg]
   [webjunk.pseudotag :refer [let-dissoc deftagfn]]

   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom with-let]]
   [reagent.dom :as rdom]))






(def tau (* 2 Math/PI))

(deftagfn tex attr
  ([s] (tex attr s {}))
  ([s opts]
   [:span (merge attr
                 {:dangerouslySetInnerHTML
                  {:__html
                   (.renderToString js/katex s (clj->js opts))}})]))

(defn interp [x y]
  (fn [t]
    (+ (* (- 1 t) x)
       (* t y))))

(defn fmt [& args] (apply cl-format nil args))

(defn fit3-dual [[s0 s1] f]
  (let [N  (interp s0 s1)
        sf (comp f N)
        [z0 d0] (sf (dual 0 1))
        [z3 d3] (sf (dual 1 1))
        z1 (+ z0 (/ d0 3))
        z2 (- z3 (/ d3 3))]
    [z0 z1 z2 z3]))
(deftagfn cubic attr [[x0 y0] [x1 y1] [x2 y2] [x3 y3] & children]
  [:path (assoc attr
                :d (fmt "M ~a,~a C ~a,~a ~a,~a ~a,~a" x0 y0 x1 y1 x2 y2 x3 y3))
   children])

(deftagfn cubic-slice attr [[s0 s1] f]
  (apply cubic attr
         (fit3-dual [s0 s1] f)))


(deftagfn cubic-slices attr [N [start end] f]
  [:g attr
   (for [i (range  N)]
     (cubic-slice [(/ i N) (/ (inc i) N)]
       (comp f (interp start end))))])

(deftagfn arc attr [z r start end]
  (for [i (range 5)
        :let [t0 (/ i 5)
              t1 (/ (inc i) 5)
              [θ0 θ1] (map (interp start end) [t0 t1])]]
    (cubic-slices (assoc attr :key i)
                  (Math/ceil (/ (- start end) (/ tau 4)))
      [θ0 θ1]
      #(+ z (* r (cis %))))))


(def timer (atom 0))

(defn timer-loop
  ([]
   (fn [time]
     (js/requestAnimationFrame (timer-loop time))))
  ([last-time]
   (fn [time]
     (let [delta (- time last-time)]
       (swap! timer + (/ delta 1500)))
     (js/requestAnimationFrame (timer-loop time)))))
(defonce the-loop
  (js/requestAnimationFrame (timer-loop)))

(defn curl []
  [:div.column
   [:svg.diagram {:viewBox "-4 -4 8 8"
                  :preserveAspectRatio "xMidYMid meet"}
    (let [t (* 4 (cos  (/ @timer 80)))
          R #(* 2 (cis %))
          f #(+ (R %) (* 1 (cis (* t %))))]
      [:<>
       [cubic-slices {:class :opaque} 40 [0 tau] f]
       [cubic-slices {:class :opaque} 60 [tau (* 8 tau)] f]
       [cubic-slices {:class :opaque} 60 [(* -7 tau) 0] f]])]])


(defn hello-world []
  [:<>
   [:svg
    [:defs [:marker {:id "arrow"
                     :viewBox "-5 -5 10 10"
                     :orient "auto"
                     :markerWidth 10
                     :markerHeight 10
                     :refX 4
                     :refY 0}
            [:path {:d "M0,0 L-2,3 L5,0 L-2,-3 Z"}]]]]
   [:main.columns.is-centered.is-multiline.is-variable.is-7
    [curl]
    ]])


(defn get-app-element []
  (gdom/getElement "app"))

(defn mount [el]
  (rdom/render [hello-world] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
